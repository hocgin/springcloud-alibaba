package in.hocg.springcloud.discovery;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author hocgin
 */
@Slf4j
@RefreshScope
@RestController
public class IndexController {
    
    @Value("${test:default}")
    private String value;
    
    @GetMapping("/worked")
    public String hello(@RequestParam String name) {
        log.info("invoked name = " + name);
        return "worked " + name + "::" + value;
    }
}
