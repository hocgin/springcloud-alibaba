package in.hocg.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author hocgin
 */
@EnableDiscoveryClient
@SpringBootApplication
public class DiscoveryApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(DiscoveryApplication.class, args);
    }
    
}
